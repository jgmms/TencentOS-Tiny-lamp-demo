#include "mcu_init.h"

#define APPLICATION_TASK_STK_SIZE       4096
k_task_t application_task;
uint8_t application_task_stk[APPLICATION_TASK_STK_SIZE];

#define BLINK_TASK_STK_SIZE       1024
k_task_t blink_task;
uint8_t blink_task_stk[BLINK_TASK_STK_SIZE];

#define DISATANCE_TASK_STK_SIZE       1024
k_task_t distance_task;
uint8_t distance_task_stk[DISATANCE_TASK_STK_SIZE];

uint8_t lastbrightness = 0;
uint8_t temp = 0;

extern void application_entry(void *arg);
extern volatile GPT_ICUserValueTypeDef GPT_ICUserValueStructure;

void blink_entry(void *arg){
		while(1){
			
			if(lastbrightness < brightness){
				for(temp = lastbrightness ; temp < brightness ; temp++){
					PWM_UpdatePwmDutycycle(PWM4, kPWM_Module_3, kPWM_PwmA, kPWM_SignedCenterAligned, temp);//更新占空比 
					PWM_SetPwmLdok(PWM4,kPWM_Control_Module_3, true);
					tos_task_delay(5);
				}
			}else if(lastbrightness > brightness){
				for(temp = lastbrightness ; temp > brightness ; temp--){
					PWM_UpdatePwmDutycycle(PWM4, kPWM_Module_3, kPWM_PwmA, kPWM_SignedCenterAligned, temp);//更新占空比 
					PWM_SetPwmLdok(PWM4,kPWM_Control_Module_3, true);
					tos_task_delay(5);
				}
			}
			lastbrightness = brightness;
		
		}	 
}


void Distance_judge_entry(void *arg){
	int distance_flag;
	
	while(1){
		Distance_measurement();//超声波测距
					
		if(distance < 1000){
			distance_flag++;
		}
		
		if(distance_flag >= 10){
			GPIO_PinWrite(GPIO2, 29, 0U);
			delay_us(500000);
			distance_flag = 0;
		}
		else if(distance_flag < 10){
			GPIO_PinWrite(GPIO2, 29, 1U);
		}			
		PRINTF("%d\n",distance);					
	}
}

int main(void)
{
    board_init();
		
    PRINTF("Welcome to TencentOS tiny(%s)\r\n", TOS_VERSION);
	
    tos_knl_init(); // TencentOS Tiny kernel initialize
    tos_task_create(&application_task, "application_task", application_entry, NULL, 2, application_task_stk, APPLICATION_TASK_STK_SIZE, 0);
		tos_task_create(&blink_task, "blink_task", blink_entry, NULL, 6, blink_task_stk, BLINK_TASK_STK_SIZE, 0);
		tos_task_create(&distance_task, "distance_task", Distance_judge_entry, NULL, 5, distance_task_stk, DISATANCE_TASK_STK_SIZE, 0);
    tos_knl_start();
    
    while (1);
	
}

