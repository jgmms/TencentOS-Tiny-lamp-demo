#include "mcu_init.h"
#include "tos_at.h"

#define PWM_SRC_CLK_FREQ CLOCK_GetFreq(kCLOCK_IpgClk)

uint8_t brightness = 0;

static void PWM_DRV_InitPwm(void)
{
    //uint16_t deadTimeVal;
    pwm_signal_param_t pwmSignal[2];
    uint32_t pwmSourceClockInHz;
    uint32_t pwmFrequencyInHz = 1000UL;
    pwmSourceClockInHz = PWM_SRC_CLK_FREQ;
    pwmSignal[0].pwmChannel       = kPWM_PwmA;
    pwmSignal[0].level            = kPWM_HighTrue;
    //pwmSignal[0].dutyCyclePercent = 100; /* 1 percent dutycycle */
    //pwmSignal[0].deadtimeValue    = deadTimeVal;
    //pwmSignal[0].faultState       = kPWM_PwmFaultState0;
    PWM_SetupPwm(PWM4, kPWM_Module_3, pwmSignal, 1, kPWM_SignedCenterAligned, pwmFrequencyInHz,
                 pwmSourceClockInHz);
}

void board_init(void)
{
	
		pwm_config_t pwmConfig;
    uint16_t pwmVal = 0;
		
    /* Init board hardware. */
    BOARD_ConfigMPU();
    BOARD_InitPins();
    BOARD_InitBootClocks();
		BOARD_BootClockRUN();
    BOARD_InitDebugConsole();

		CLOCK_SetDiv(kCLOCK_AhbDiv, 0x2); /* Set AHB PODF to 2, divide by 3 */
    CLOCK_SetDiv(kCLOCK_IpgDiv, 0x3); /* Set IPG PODF to 3, divede by 4 */
		/*XBARA Init*/
		XBARA_Init(XBARA1);
    XBARA_SetSignalsConnection(XBARA1, kXBARA1_InputLogicHigh, kXBARA1_OutputFlexpwm4Fault0);
    XBARA_SetSignalsConnection(XBARA1, kXBARA1_InputLogicHigh, kXBARA1_OutputFlexpwm4Fault1);
    XBARA_SetSignalsConnection(XBARA1, kXBARA1_InputLogicHigh, kXBARA1_OutputFlexpwm1234Fault2);
    XBARA_SetSignalsConnection(XBARA1, kXBARA1_InputLogicHigh, kXBARA1_OutputFlexpwm1234Fault3);
	
		PWM_GetDefaultConfig(&pwmConfig);
		pwmConfig.reloadLogic = kPWM_ReloadPwmFullCycle;//全周期更新
		if (PWM_Init(PWM4, kPWM_Module_3, &pwmConfig) == kStatus_Fail)
		{
				PRINTF("PWM initialization failed\n");
				//return 1;
		}
		PWM_DRV_InitPwm();
		PWM_StartTimer(PWM4, kPWM_Control_Module_3);
		HC_SR04_Init();
		GPT_Config();
}

void SysTick_Handler(void)
{
    if (tos_knl_is_running()) {
        tos_knl_irq_enter();
        tos_tick_handler();
        tos_knl_irq_leave();
    }
}

/* LPUART2_IRQn interrupt handler */
void LPUART2_IRQHandler(void) {
    uint8_t data;
    
    tos_knl_irq_enter();

    /* If new data arrived. */
    if ((kLPUART_RxDataRegFullFlag)&LPUART_GetStatusFlags(LPUART2))
    {
        data = LPUART_ReadByte(LPUART2);
        tos_at_uart_input_byte(data);
    }
    
    tos_knl_irq_leave();
}

/* LPUART4_IRQn interrupt handler */
void LPUART4_IRQHandler(void) {
    uint8_t data;
    
    tos_knl_irq_enter();

    /* If new data arrived. */
    if ((kLPUART_RxDataRegFullFlag)&LPUART_GetStatusFlags(LPUART4))
    {
        data = LPUART_ReadByte(LPUART4);
        tos_at_uart_input_byte(data);
    }
    
    tos_knl_irq_leave();
}

