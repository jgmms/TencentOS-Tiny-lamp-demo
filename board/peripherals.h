/***********************************************************************************************************************
 * This file was generated by the MCUXpresso Config Tools. Any manual edits made to this file
 * will be overwritten if the respective MCUXpresso Config Tools is used to update this file.
 **********************************************************************************************************************/

#ifndef _PERIPHERALS_H_
#define _PERIPHERALS_H_

/***********************************************************************************************************************
 * Included files
 **********************************************************************************************************************/
#include "fsl_common.h"
#include "fsl_pwm.h"

#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */

/***********************************************************************************************************************
 * Definitions
 **********************************************************************************************************************/
/* Definitions for BOARD_InitPeripherals functional group */
/* Definition of peripheral ID */
#define PWM2_PERIPHERAL PWM2
/* Definition of submodule 0 ID */
#define PWM2_SM0 kPWM_Module_0
/* Definition of clock source of submodule 0 frequency in Hertz */
#define PWM2_SM0_SM_CLK_SOURCE_FREQ_HZ 150000000U
/* Definition of submodule 0 counter clock source frequency in Hertz - PWM2_SM0_SM_CLK_SOURCE_FREQ_HZ divided by prescaler */
#define PWM2_SM0_COUNTER_CLK_SOURCE_FREQ_HZ 150000000U
/* Definition of submodule 0 counter (PWM) frequency in Hertz */
#define PWM2_SM0_COUNTER_FREQ_HZ 10000U
/* Definition of submodule 0 channel A ID */
#define PWM2_SM0_A kPWM_PwmA
/* Definition of submodule 0 channel B ID */
#define PWM2_SM0_B kPWM_PwmB
/* Definition of submodule 0 channel X ID */
#define PWM2_SM0_X kPWM_PwmX
/* Definition of fault Fault0 ID */
#define PWM2_F0_FAULT0 kPWM_Fault_0
/* Definition of fault Fault1 ID */
#define PWM2_F0_FAULT1 kPWM_Fault_1
/* Definition of fault Fault2 ID */
#define PWM2_F0_FAULT2 kPWM_Fault_2
/* Definition of fault Fault3 ID */
#define PWM2_F0_FAULT3 kPWM_Fault_3

/***********************************************************************************************************************
 * Global variables
 **********************************************************************************************************************/
extern pwm_config_t PWM2_SM0_config;

extern pwm_signal_param_t PWM2_SM0_pwm_function_config[1];
extern const pwm_fault_input_filter_param_t PWM2_faultInputFilter_config;
extern const pwm_fault_param_t PWM2_Fault0_fault_config;
extern const pwm_fault_param_t PWM2_Fault1_fault_config;
extern const pwm_fault_param_t PWM2_Fault2_fault_config;
extern const pwm_fault_param_t PWM2_Fault3_fault_config;

/***********************************************************************************************************************
 * Initialization functions
 **********************************************************************************************************************/

void BOARD_InitPeripherals(void);

/***********************************************************************************************************************
 * BOARD_InitBootPeripherals function
 **********************************************************************************************************************/
void BOARD_InitBootPeripherals(void);

#if defined(__cplusplus)
}
#endif

#endif /* _PERIPHERALS_H_ */
