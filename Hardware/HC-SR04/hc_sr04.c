#include "hc_sr04.h"
#include "fsl_gpt.h"

// 定时器输入捕获用户自定义变量结构体定义
volatile GPT_ICUserValueTypeDef GPT_ICUserValueStructure = {0,0,0,0,0};
volatile uint64_t timer = 0;
volatile int distance = 0;

  /**
* @brief  设置中断优先级分组  
* @param  PriorityGroup:中断优先级分组
*/   
void Set_NVIC_PriorityGroup(PriorityGroup_Type PriorityGroup) 
{
  NVIC_SetPriorityGrouping((uint32_t)PriorityGroup); //设置中断优先级分组
}


 /**
* @brief  设置中断编号的中断优先级  
* @param  IRQn:中断编号
* @param  PreemptPriority:抢占优先级
* @param  SubPriorit:子优先级
*/
void set_IRQn_Priority(IRQn_Type IRQn,PreemptPriority_Type PreemptPriority, SubPriority_Type SubPriorit)
{
  uint32_t PriorityGroup = 0;   //保存中断优先级分组
  uint32_t Priority_Encode = 0; //保存中断优先级编码
  PriorityGroup = NVIC_GetPriorityGrouping();//获得当前中断优先级分组
  Priority_Encode = NVIC_EncodePriority (PriorityGroup,(uint32_t)PreemptPriority,(uint32_t)SubPriorit);//得到中断优先级编码
  
  NVIC_SetPriority(IRQn, Priority_Encode);//设置中断编号的中断优先级
}



/*配置GPT工作模式*/  
void GPT_Config(void)
{

  gpt_config_t gptConfig;
  
  
  /*GPT的时钟设置*/
  CLOCK_SetMux(kCLOCK_PerclkMux, EXAMPLE_GPT_CLOCK_SOURCE_SELECT);
  CLOCK_SetDiv(kCLOCK_PerclkDiv, EXAMPLE_GPT_CLOCK_DIVIDER_SELECT);
  
  /*初始化GPT*/
  GPT_GetDefaultConfig(&gptConfig);
  GPT_Init(GPT2, &gptConfig);
  
  
  /* 设置时钟分频(1-4096) */
  GPT_SetClockDivider(GPT2, 1);


  /*设置位输入模式*/
  GPT_SetInputOperationMode(GPT2,kGPT_InputCapture_Channel2,kGPT_InputOperation_FallEdge);
  /*使能输入捕获中断*/
  GPT_EnableInterrupts(GPT2, kGPT_InputCapture2InterruptEnable);
  /*使能溢出中断*/
  GPT_EnableInterrupts(GPT2,kGPT_RollOverFlagInterruptEnable);
  
  /*设置中断优先级,*/
  set_IRQn_Priority(GPT2_IRQn,Group4_PreemptPriority_6, Group4_SubPriority_0);
  /*使能中断*/
  EnableIRQ(GPT2_IRQn);

  /* 开启定时器 */
  GPT_StartTimer(GPT2);
	PRINTF("GPT2 Start\n");
}

/*定义中断服务函数*/
void EXAMPLE_GPT_IRQHandler(void)
{
  /*
  *当要被捕获的信号的周期大于定时器的最长定时时，定时器就会溢出，产生更新中断
  *这个时候我们需要把这个最长的定时周期加到捕获信号的时间里面去
  */ 
   if ( GPT_GetStatusFlags(GPT2,kGPT_RollOverFlag) != false )               
   {	
      if ( GPT_ICUserValueStructure.Capture_StartFlag != 0 )
      {
        GPT_ICUserValueStructure.Capture_Period ++;	
      }
      GPT_ClearStatusFlags(GPT2, kGPT_RollOverFlag); 		
   }
  
   /*捕获中断*/ 
  if (GPT_GetStatusFlags(GPT2,kGPT_InputCapture2Flag) != false)
  {
    
      if(GPT_ICUserValueStructure.Capture_FinishFlag != 1)
      {
        /*第一次捕获*/ 
        if ( GPT_ICUserValueStructure.Capture_StartFlag == 0 )
        {
          /*清除溢出次数*/
          GPT_ICUserValueStructure.Capture_Period = 0;
          
          /*读取当前计数值*/ 
          GPT_ICUserValueStructure.Capture_CcrValue_1 = GPT_GetInputCaptureValue(GPT2,kGPT_InputCapture_Channel2);
          /*当第一次捕获到上升沿之后，就把捕获边沿配置为上升沿*/ 
          GPT_SetInputOperationMode(GPT2,kGPT_InputCapture_Channel2,kGPT_InputOperation_RiseEdge);
          /*开始捕获标志置1*/ 			
          GPT_ICUserValueStructure.Capture_StartFlag = 1;			
        }
        /*上升沿捕获中断,第二次捕获*/ 
        else 
        {
          /*获取捕获比较寄存器的值，这个值就是捕获到的高电平的时间的值*/ 
          GPT_ICUserValueStructure.Capture_CcrValue_2 = GPT_GetInputCaptureValue(GPT2,kGPT_InputCapture_Channel2);   

          /*当第二次捕获到上升沿之后，就把捕获边沿配置为下降沿，好开启新的一轮捕获*/ 
          GPT_SetInputOperationMode(GPT2,kGPT_InputCapture_Channel2,kGPT_InputOperation_FallEdge);
          /*开始捕获标志清0*/ 		
          GPT_ICUserValueStructure.Capture_StartFlag = 0;
          /*捕获完成标志置1	*/ 		
          GPT_ICUserValueStructure.Capture_FinishFlag = 1;		
         }
       }

     GPT_ClearStatusFlags(GPT2, kGPT_InputCapture2Flag);
  }      
    
}


void HC_SR04_Init(void){
    GPIO_PinWrite(TRIG_GPIO,TRIG_GPIO_PIN, 0U);
    delay_us(5);

// trig引脚电位稳定后，给一个10微秒的高电平触发信号
    GPIO_PinWrite(TRIG_GPIO,TRIG_GPIO_PIN, 1U);
    delay_us(10);
    GPIO_PinWrite(TRIG_GPIO,TRIG_GPIO_PIN, 0U);
		PRINTF("HC_SR04_Init OK\n");
}

void Distance_measurement(void){
    
    GPIO_PinWrite(TRIG_GPIO,TRIG_GPIO_PIN, 1U);
    delay_us(20);
    GPIO_PinWrite(TRIG_GPIO,TRIG_GPIO_PIN, 0U);

    if(GPT_ICUserValueStructure.Capture_FinishFlag)
    {
        /*得到计数值，timer 为64位数据，32位很可能会溢出*/
        timer = GPT_ICUserValueStructure.Capture_Period * 0xffffffff; 
        timer += GPT_ICUserValueStructure.Capture_CcrValue_2;         
        timer -= GPT_ICUserValueStructure.Capture_CcrValue_1;
        
        /*将计数值转化为时间，单位（ms）*/
        timer = (172*timer) / ((EXAMPLE_GPT_CLK_FREQ)/1000);
				distance =(int)(timer);
			
       // PRINTF("the distance is: %d m \r\n",distance);
        PRINTF("%d\n",distance);
				
        GPT_ICUserValueStructure.Capture_FinishFlag = 0;
				delay_us(100000);

    }
}