#ifndef _HC_SR04_H_
#define _HC_SR04_H_

#include "fsl_common.h"
#include "fsl_iomuxc.h"
#include "fsl_gpio.h"
#include "pin_mux.h"
#include "fsl_gpt.h"
#include "mcu_init.h"
#include "delay.h"

#define TRIG_GPIO          GPIO2
#define TRIG_GPIO_PIN      28U

/* 选择 IPG Clock as PERCLK_CLK clock source */
#define EXAMPLE_GPT_CLOCK_SOURCE_SELECT (0U)
/* Clock divider for PERCLK_CLK clock source */
#define EXAMPLE_GPT_CLOCK_DIVIDER_SELECT (0U)

/*GPT 时钟分频(1-4096)*/
#define GPT_DIVIDER 1

/* 得到GPT定时器的计数频率*/
#define EXAMPLE_GPT_CLK_FREQ ( (CLOCK_GetFreq(kCLOCK_IpgClk)\
      / (EXAMPLE_GPT_CLOCK_DIVIDER_SELECT + 1U))/GPT_DIVIDER )
			
#define COUNGER_NUMBER  ((TIME_ms/1000.0)*EXAMPLE_GPT_CLK_FREQ)
#define TIME_ms 1000

#define EXAMPLE_GPT_IRQHandler GPT2_IRQHandler

/*定义中断优先级分组 */
typedef enum PriorityGroup{

  Group_0 = 0,
  Group_1,
  Group_2,
  Group_3,
  Group_4,
  Group_5,
  Group_6,
  Group_7,
}PriorityGroup_Type;

/*定义抢占优先级*/
typedef enum PreemptPriority {
  Group0_PreemptPriority_0 = 0,
  Group0_PreemptPriority_1,
  Group0_PreemptPriority_2,
  Group0_PreemptPriority_3,
  Group0_PreemptPriority_4,
  Group0_PreemptPriority_5,
  Group0_PreemptPriority_6,
  Group0_PreemptPriority_7,
  Group0_PreemptPriority_8,
  Group0_PreemptPriority_9,
  Group0_PreemptPriority_10,
  Group0_PreemptPriority_11,
  Group0_PreemptPriority_12,
  Group0_PreemptPriority_13,
  Group0_PreemptPriority_14,
  Group0_PreemptPriority_15, 
	
	
	
	Group1_PreemptPriority_0 = 0,
  Group1_PreemptPriority_1,
  Group1_PreemptPriority_2,
  Group1_PreemptPriority_3,
  Group1_PreemptPriority_4,
  Group1_PreemptPriority_5,
  Group1_PreemptPriority_6,
  Group1_PreemptPriority_7,
  Group1_PreemptPriority_8,
  Group1_PreemptPriority_9,
  Group1_PreemptPriority_10,
  Group1_PreemptPriority_11,
  Group1_PreemptPriority_12,
  Group1_PreemptPriority_13,
  Group1_PreemptPriority_14,
  Group1_PreemptPriority_15,

  Group2_PreemptPriority_0 = 0,
  Group2_PreemptPriority_1,
  Group2_PreemptPriority_2,
  Group2_PreemptPriority_3,
  Group2_PreemptPriority_4,
  Group2_PreemptPriority_5,
  Group2_PreemptPriority_6,
  Group2_PreemptPriority_7,
  Group2_PreemptPriority_8,
  Group2_PreemptPriority_9,
  Group2_PreemptPriority_10,
  Group2_PreemptPriority_11,
  Group2_PreemptPriority_12,
  Group2_PreemptPriority_13,
  Group2_PreemptPriority_14,
  Group2_PreemptPriority_15,

  Group3_PreemptPriority_0 = 0,
  Group3_PreemptPriority_1,
  Group3_PreemptPriority_2,
  Group3_PreemptPriority_3,
  Group3_PreemptPriority_4,
  Group3_PreemptPriority_5,
  Group3_PreemptPriority_6,
  Group3_PreemptPriority_7,
  Group3_PreemptPriority_8,
  Group3_PreemptPriority_9,
  Group3_PreemptPriority_10,
  Group3_PreemptPriority_11,
  Group3_PreemptPriority_12,
  Group3_PreemptPriority_13,
  Group3_PreemptPriority_14,
  Group3_PreemptPriority_15,



  Group4_PreemptPriority_0 = 0,
  Group4_PreemptPriority_2,
  Group4_PreemptPriority_3,
  Group4_PreemptPriority_4,
  Group4_PreemptPriority_5,
  Group4_PreemptPriority_6,
  Group4_PreemptPriority_7,
	
  
  Group5_PreemptPriority_0 = 0,
  Group5_PreemptPriority_1,
  Group5_PreemptPriority_2,
  Group5_PreemptPriority_3,
  
  Group6_PreemptPriority_0 = 0,
  Group6_PreemptPriority_1,
  
  Group7_PreemptPriority_0 = 0
}PreemptPriority_Type;

/*定义子优先级*/
typedef enum SubPriority {

  Group0_SubPriority_0 = 0,
	Group1_SubPriority_0 = 0,
	Group2_SubPriority_0 = 0,
	Group3_SubPriority_0 = 0,
			
  Group4_SubPriority_0 = 0,
  Group4_SubPriority_1,
  
  Group5_SubPriority_0 = 0,
  Group5_SubPriority_1,
  Group5_SubPriority_2,
  Group5_SubPriority_3,
  
  
  Group6_SubPriority_0 = 0,
  Group6_SubPriority_2,
  Group6_SubPriority_3,
  Group6_SubPriority_4,
  Group6_SubPriority_5,
  Group6_SubPriority_6,
  Group6_SubPriority_7,
  
  Group7_SubPriority_0 = 0,
  Group7_SubPriority_1,
  Group7_SubPriority_2,
  Group7_SubPriority_3,
  Group7_SubPriority_4,
  Group7_SubPriority_5,
  Group7_SubPriority_6,
  Group7_SubPriority_7,
  Group7_SubPriority_8,
  Group7_SubPriority_9,
  Group7_SubPriority_10,
  Group7_SubPriority_11,
  Group7_SubPriority_12,
  Group7_SubPriority_13,
  Group7_SubPriority_14,
  Group7_SubPriority_15
  
}SubPriority_Type;
// 定时器输入捕获用户自定义变量结构体声明
typedef struct
{   
	uint8_t   Capture_FinishFlag;   // 捕获结束标志位
	uint8_t   Capture_StartFlag;    // 捕获开始标志位
	uint32_t  Capture_CcrValue_1;     // 捕获寄存器的值
    uint32_t   Capture_CcrValue_2;     // 捕获寄存器的值
	uint16_t  Capture_Period;       // 定时器溢出次数 
}GPT_ICUserValueTypeDef;

void Set_NVIC_PriorityGroup(PriorityGroup_Type PriorityGroup) ;
void set_IRQn_Priority(IRQn_Type IRQn,PreemptPriority_Type PreemptPriority, SubPriority_Type SubPriorit);
void EXAMPLE_GPT_IRQHandler(void);
void GPT_Config(void);
void HC_SR04_Init(void);
void Distance_measurement(void);

#endif