# 基于TencentOS Tiny的台灯demo

#### 介绍
具体介绍见：http://https://cloud.tencent.com/developer/article/1955427

基于TencentOS Tiny的台灯demo，使用全新硬件平台EVB-AIoT开发板
![EVB-AIoT开发板](picture/d99284351546571dd9461e7a26b0a101.png)
- 核心板采用的RT1062处理器属于i.MX RT 系列 MCU，是由 NXP 推出的跨界处理器，跨界是指该系列MCU的定位既非传统的微控制器、也非传统的微处理器，i.MX RT 系列 MCU 则综合了两者的优势，既具备高频率（最高主频600M）、高处理性能，也具备中断响应迅速、实时性高的特点。
- 1M RAM 16M SDRAM  64MB qspi flash 128MB spi flash。
- 板载Type-C接口CMSIS DAP仿真器。
- 板载PCIE接口，可扩展4G类物联网模组。
- 板载物联网俱乐部WAN Interface接口，可支持NB-IoT、WiFi、4G cat1、LoRa等模组。
- 板载物联网俱乐部E53 Interface接口，可扩展全系E53传感器。
- 板载标准24P DVP摄像头接口，可支持最高500万像素摄像头。
- 板载RGB显示接口，可转换HDMI输出。
- 板载高性能音频解码芯片，可做语音识别测试。
- 预留SD卡、用户按键、SPI Flash。
#### 功能介绍
![功能介绍](picture/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202022-03-16%20104747.png)
- 坐姿检测

通过HC-SR04超声波模块实时检测使用者与台灯之间的距离判断坐姿，提醒使用者调整坐姿。
- 暗环境开灯提醒

再也不会忘记太阳落山时开灯了。。。
- 柔和调光

现在市面的大多数台灯都只是分几档亮度，拥有无极调光的台灯价格都不太友好。于是增加了PWM无极调光，光线柔和变换，减轻用眼负担。
- 小程序控制

TencentOS-Tiny作为物联网操作系统依托腾讯强大的物联网云平台可以让项目更简单的接入微信小程序，不用下载第三方APP即可做到远程遥控。
#### 软件架构
![总体任务架构](picture/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202022-03-16%20104507.png)

